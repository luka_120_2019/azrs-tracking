#Projekat azrs-tracking

U ovom projektu smo demonstrirali koriscenje alata za unapredjivanje kavliteta projekta Catan koji je radjen iz predmete razvoj softvers 2022/2023.

Korisceni alati su:
- git
- git-hook
- Valgrind
- CppCheck
- ClangFormat
- ClangTidy
- Conan
- CMake
- Doxygen

